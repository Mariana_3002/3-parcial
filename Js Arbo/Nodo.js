class Nodo{
    constructor(padre = null, nivel = null, posicion= null, valor, nombre){
        this.padre = padre;
        this.nivel = nivel;
        this.posicion = posicion;
        this.valor = valor;
        this.nombre = nombre;
    }

    hijoI(padre, nivel, posicion, valor, nombre){
        var nodo = new Nodo( padre, nivel, posicion, valor, nombre);
        return nodo;
    }
    hijoD(padre, nivel, posicion, valor, nombre){
        var nodo = new Nodo(padre, nivel, posicion, valor, nombre);
        return nodo;
    }
}