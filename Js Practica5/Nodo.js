class Nodo{
    constructor(padre = null, nivel = null, posicion= null, valor, nombre, caracteristicas){
        this.padre = padre;
        this.nivel = nivel;
        this.posicion = posicion;
        this.valor = valor;
        this.nombre = nombre;
        this.caracteristicas = {
            "Nodo 1": {
                "Nombre": "A",
                "valor": "+",
                "Posicion": "0",
                "nivel": "0",
                "gustos": ["Musica", "Salir de fiesta"],
            },
            "Nodo 2": {
                "Nombre": "B",
                "Valor": "*",
                "Posicion": "1",
                "nivel": "1",
                "gustos": ["Musica", "ir al cine", "Tocar la guitarra"]
        }
    }
}

    hijoI(padre, nivel, posicion, valor, nombre){
        var nodo = new Nodo( padre, nivel, posicion, valor, nombre);
        return nodo;
    }
    hijoD(padre, nivel, posicion, valor, nombre){
        var nodo = new Nodo(padre, nivel, posicion, valor, nombre);
        return nodo;
    }
}